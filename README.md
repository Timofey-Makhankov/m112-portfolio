# Aufgaben

## Übung 1

1. -> cd
2. -> cd /var/log
3. -> cd /etc/udev
4. -> cd ..
5. -> cd network
6. -> cd ../../dev

## Übung 2

1. -> mkdir Docs
2. -> touch file{1..10}.txt
3. -> rm \*3\*.txt
4. -> rm file{2,4,7}.txt
5. -> rm \*.txt
6. -> mkdir Files
7. -> touch file{1..10}.txt
8. -> cp -R Files/ Files2/
9. -> cp -R Files/ Files2/Files3
10. -> mv Files/ Files1/
11. -> rm -rf Files*

## Übung 3

1. cd ~
2. cd ~-
3. cd ~timofey
4. cd ~+

## Übung 4

### A

1. -> grep obelix dummy.txt
2. -> grep 2 dummy.txt
3. -> grep e dummy.txt
4. -> grep -v gamma dummy.txt
5. -> grep -E "[123]" dummy.txt

### B

1. -> cut -d ':' -f1 dummy.txt
2. ->
3. ->

