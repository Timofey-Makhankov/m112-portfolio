#!/bin/bash

# Author - Timofey Makhankov
# Project A Script

# Creates the Folder for each class with the template files
# @param - File Location
# @param - Folder Name

function create_school_dir {
        para_file_name=$1
        para_name=$2
        mkdir $para_name
        while read line
        do
                mkdir $para_name/$line
                cp ./template/* $para_name/$line/
        done < $para_file_name
}

# Returns a name from a File Location
# @param - File Location

function get_file_name {
        FILENAME=$1
        ONLY_NAME="${FILENAME##*/}"
        NAME="${ONLY_NAME%.*}"
        echo $NAME
}

# Check if a Parameter is given
if [ $# != 0 ]; then
	FILENAME=$1
	NAME=$(get_file_name $FILENAME)
	create_school_dir $FILENAME $NAME
	exit 0
else
	# check if 'classes' and 'template' folder exsists
	if [[ -d "classes" && -d "template" ]]; then
		for file in ./classes/*
		do
			FILENAME=$file
			NAME=$(get_file_name $FILENAME)
			create_school_dir $FILENAME $NAME
		done
		exit 0
	else
		echo "classes or template Folder was not found"
		exit 1
	fi
fi
