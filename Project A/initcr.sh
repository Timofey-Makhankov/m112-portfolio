#!/bin/bash

# Author - Timofey Makhankov
# Project A Script

CLASS_DIR="classes"
TEMPLATE_DIR="template"

# create Folders
mkdir $CLASS_DIR
mkdir $TEMPLATE_DIR

# create temp files
touch $TEMPLATE_DIR/Datei-{1.txt,2.docx,3.pdf}

# write to file
echo "Blattner
Boshtraj
Cecutti
Daniels
Erdinc
Haradini
Kohler
Kos
Meile
Meyer
Muggli
Mueller
Nguyen
Pfrender
Schäfli
Vennemann
Weibel
Bajra" >> $CLASS_DIR/M122-AP22b.txt

# write to file
echo "Andreatta
Benz
Blau
Chandler
Duersteler
Fanzun
Haueter
Kohl
Mohler
Morgenthaler
Odermatt
Pizzi
Seiler
Steinauer
Steiner
Sureskumar
Ugur
Weinrich" >> $CLASS_DIR/M122-AP22d.txt
